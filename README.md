# Keithley Picoammeter Readout

### Description
This project hosts the library to control Keithley 2614 and Keithley 6485.
The whole project is written using AutoIt Scripting Language to allow the best integration with Windows 10 software.

### Usage Case Keithley 2614B
A standard case usage of the function here is:

```
openport()
$current_reading = read_keithley_2614B(1)

```

which will start the RS232 communication and then return the AVG of 200 current points red on the channel B of the Keithley 2614B.

Several parameters of the RS232 communication can be adjusted by directly editing the keithley.au3 library file.

### Usage case Keithley 6485
A standard case usage is 

```
openport()
$current_reading = read_keithley_6485(<port_number>,<picoammeter_range>)

```

which will start the RS232 communication and then return the AVG of 200 current points red on the Keithley 6485 having port number `port_number`. The Keithley 6485 range will be set to `picoammeter_range`.

