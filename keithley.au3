;; DAQ on version 1.0


#include <File.au3>
#include <Timers.au3>
#include <Excel.au3>
#include <Array.au3>
#include <FontConstants.au3>
#include <Date.au3>
#include <GUIConstantsEx.au3>
#include <GuiEdit.au3>
#include <EditConstants.au3>
#Include <ScrollBarConstants.au3>
#include <GuiTab.au3>
#include <GuiButton.au3>
#include "E:\GEM_AC\autoit_PostHenning\Autoit_Extensions\CommMG.au3"
#include "E:\GEM_AC\autoit_PostHenning\Autoit_Extensions\AmptekXrayControl.au3"
#include "E:\GEM_AC\autoit_PostHenning\Autoit_Extensions\MiniX2Control.au3"


Func openport()
	$ret = do_ports_exist()  ; check if the list of com_port_available can be opened and communicating to
	If $ret = 1 Then
		$ret = port_identify() ;
	Else
		Exit
	EndIf

	;If ($port > 0) Then
	If ($com_port_counter > 0) Then   ; Means that we have detected Counter on the COMM Port = com_port_counter
		$MsComm.CommPort = $com_port_counter
		$MsComm.Settings = "115200,N,8,1"
		$MsComm.Handshaking = 0
		$MsComm.InBufferSize = 1024
		$MsComm.InputLen = 1
		$MsComm.PortOpen = 1
		If @error Then MsgBox(48 + 262144, "COM Error", "@error is set to COM error number." & @CRLF & "@error = " & @error)
	EndIf
	;If ($portHV > 0) Then
	If ($com_port_HV_1 > 0) Then ; Means that we have detected HV Left on the COMM Port = com_port_counter
		$MsCommHV.CommPort = $com_port_HV_1
		$MsCommHV.Settings = "9600,N,8,1"
		$MsCommHV.Handshaking = 0
		$MsCommHV.InBufferSize = 1024
		$MsCommHV.InputLen = 1
		$MsCommHV.PortOpen = 1
		$MsCommHV.Output = @CR ;  sync, as given in manual
		Sleep(5)
		$MsCommHV.Output = @LF ;  sync, as given in manual
		Sleep(300)
		_MsgOutHV("W=1")
		$modul_number = _MsgInHV()
		;ConsoleWrite("W=1 return: " & $modul_number & @CRLF)
		_MsgOutHV("#")
		$modul_number = _MsgInHV()
		;ConsoleWrite("Module #: " & $modul_number & @CRLF)
;~ 		$MsCommHV.Output = " " & @CRLF ; sync, as give in manual
	EndIf
EndFunc   ;==>openport



Func closeport()

	If ($MsComm.PortOpen = 1) Then
		$MsComm.PortOpen = 0
	EndIf
	If ($MsCommHV.PortOpen = 1) Then
		$MsCommHV.PortOpen = 0
	EndIf
	Print("Serialport(s) closed" & @CRLF)
EndFunc   ;==>closeport
Func _MsgOut($str)
	$MsComm.OutBufferCount = 0
	$MsComm.InBufferCount = 0

	If $MsComm.PortOpen = True Then
		$MsComm.Output = $str
;~ 		ConsoleWrite("Write: >" & $str & "<" & @CRLF)
	EndIf
	$MsComm.InputLen = 0 ; Antwort e000..0000 löschen ?
	;Sleep(500)
EndFunc   ;==>_MsgOut



Func do_ports_exist()
	Print("do_ports_exist()" & @CRLF)
	$all_exist = 1 ;
	$all_missing = 1 ;
	For $portnum In $com_port_available
		Print("Attempt to communicate with CommPort:" & $portnum & @CRLF)
		$MsComm.CommPort = $portnum
		$MsComm.Settings = "115200,N,8,1"
		$MsComm.Handshaking = 0
		;$MsComm.InBufferSize = 1024
		$MsComm.InputLen = 1
		$MsComm.PortOpen = 1
		$err_com = @error
		Print("Error com:" & $err_com & @CRLF)
		If $err_com = 0 Then
			$all_missing = 0 ; port can be opened
			Print("Port:" & $portnum & " opened" & @CRLF)
			$MsComm.PortOpen = 0
			Print("Port:" & $portnum & " closed" & @CRLF)
		Else
			If ($err_com = -2146820286) Then ; invalid port number
				Print("Port:" & $portnum & ": invalid port number" & @CRLF)
				$all_exist = 0
			Else
				; different error , maybe already opened
				Print("Port:" & $portnum & ": other error :" & $err_com & @CRLF)
				$all_missing = 0
			EndIf
		EndIf
		;		 $MsComm.PortOpen = 0
	Next

	If $all_exist = 1 Then Return 1
	If $all_missing = 1 Then Return 0
	If $all_exist = 0 And $all_missing = 0 Then Return -1

	Return -2 ; ports do not exist
EndFunc   ;==>do_ports_exist





Func port_identify() ;

	Print(@CRLF)
	Print(@CRLF &"===============================" & @CRLF)
	Print("Identification of the CommPorts" & @CRLF)
	Print("===============================" & @CRLF)


	; identify HV left
	Print("Identifying HV left..." & @CRLF)
	For $i = 0 To 3 Step 1
		If $com_port_used[$i] = -1 Then
			Print($com_port_available[$i] & @CRLF)
			$MsCommHV.CommPort = $com_port_available[$i]
			$MsCommHV.Settings = "9600,N,8,1"
			$MsCommHV.Handshaking = 0
			$MsCommHV.InBufferSize = 1024
			$MsCommHV.InputLen = 1
			$MsCommHV.PortOpen = 1

			$err_com = @error
			If $err_com = 0 Then
				; check for correct answer
				$MsCommHV.Output = @CR ;  sync, as given in manual
				Sleep(5)
				$MsCommHV.Output = @LF ;  sync, as given in manual
				Sleep(300)
				$ret = _MsgOutHV("#")
				If $ret = -10 Then
					; can't read echo , not a HV module
					$MsCommHV.PortOpen = 0 ; close port
					ContinueLoop
				EndIf
				$modul_number = _MsgInHV()
				Print("Module #: " & $modul_number & @CRLF)
				If $modul_number = "487970;2.08;5000V;2mA" Then
					; port identified
					$com_port_used[$i] = 1 ; HV left
					$com_port_HV_1 = $com_port_available[$i]
					Print("== --> Found HV Left @ " &  $com_port_HV_1 &  @CRLF )
					Print("-------------------------------" & @CRLF & @CRLF)
					$MsCommHV.PortOpen = 0 ; close port
					ExitLoop ; da identifiziert, gehe zum nächsten Objekt.
				EndIf
				$MsCommHV.PortOpen = 0 ;
			Else
				; error , since it exists it can only be already open(?)
				If $err_com Then MsgBox(48 + 262144, "COM Error", "@error is set to COM error number." & @CRLF & "@error = " & $err_com)
			EndIf
		EndIf
	Next



	Print("Identifying Picoam Keithley 6485 1252668..." & @CRLF)
	For $i = 0 To 3
		If $com_port_used[$i] = -1 Then
			$port = $com_port_available[$i]
			$err_msg = "Setport failed"
			_CommSwitch($port)
			_CommSetport($port, $err_msg, 57600, 8, 0, 1, 0, 0, 0)
			snd("*CLS")
			sleep(300)
			snd("*IDN?")
			$idn = pico_read()
			Print("PICO SAID : " & $idn)
			IF StringInStr($idn, "KEITHLEY INSTRUMENTS INC.,MODEL 6485,1252668,B04") > 0 Then ;; The port com_port_available[$i] talks to Keithley
				$com_port_picoam_1252668 = $com_port_available[$i]
				$com_port_used[$i] = 2 ; set it to Picoam old
				Print("== --> Found Picoammeter @ " &  $com_port_picoam_1252668 &  @CRLF )
				Print("-------------------------------" & @CRLF)
				ExitLoop
			EndIf
		EndIf
	Next

	Print("Identifying Picoam Keithley 6485 4005642..." & @CRLF)
	For $i = 0 To 3
		If $com_port_used[$i] = -1 Then
			$port = $com_port_available[$i]
			$err_msg = "Setport failed"
			_CommSwitch($port)
			_CommSetport($port, $err_msg, 57600, 8, 0, 1, 0, 0, 0)
			snd("*CLS")
			sleep(300)
			snd("*IDN?")
			$idn = pico_read()
			Print("PICO SAID : " & $idn)
			IF StringInStr($idn, "KEITHLEY INSTRUMENTS INC.,MODEL 6485,4005642,C01") > 0 Then ;; The port com_port_available[$i] talks to Keithley
				$com_port_picoam_4005642 = $com_port_available[$i]
				$com_port_used[$i] = 3 ; set it to Picoam new
				Print("== --> Found Picoammeter @ " &  $com_port_picoam_4005642 &  @CRLF )
				Print("-------------------------------" & @CRLF & @CRLF)
				ExitLoop
			EndIf
		EndIf
	Next



	Print("Identifying Picoam Keithley 2614B..." & @CRLF)
	For $i = 0 To 3
		If $com_port_used[$i] = -1 Then
			$port = $com_port_available[$i]
			$err_msg = "Setport failed"
			_CommSwitch($port)
			_CommSetport($port, $err_msg, 57600, 8, 0, 1, 0, 0, 0)
			snd("*CLS")
			sleep(300)
			snd("*IDN?")
			$idn = pico_read()
			Print("PICO SAID : " & $idn)
			IF StringInStr($idn, "Keithley Instruments Inc., Model 2614B, 4019406, 3.0.1") > 0 Then ;; The port com_port_available[$i] talks to Keithley
				$com_port_picoam_2614B = $com_port_available[$i]
				$com_port_used[$i] = 4 ; set it to Picoam new
				Print("== --> Found Picoammeter @ " &  $com_port_picoam_2614B &  @CRLF )
				Print("-------------------------------" & @CRLF & @CRLF)
				ExitLoop
			EndIf
		EndIf
	Next


	_Commcloseport(True)


	Print(@CRLF)
	Print("============" & @CRLF & "Port mapping" & @CRLF & "============" & @CRLF)
	For $i = 0 To 3   ; Our logic for ports is com_port_available[i] is connected to $com_device_name[com_port_used[i]]
		Dim $com_device_name[5] = ["", "HV left","Keithley 6485 1252668","Keithley 6485 4005642","Keithley 2614B"]
		If $com_port_used[$i] = -1 Then
			Print("com_port_available[" & $i &"] = COMM" & $com_port_available[$i] &  " is connected to Unknown" & @CRLF)
		Else
			Print("com_port_available[" & $i &"] = COMM" & $com_port_available[$i] & " is connected to " & $com_device_name[$com_port_used[$i]]& @CRLF)
		EndIf
	Next
	If $com_port_picoam_1252668 = -1 Then
		MsgBox($MB_ICONWARNING,"Device(s) not found"& @CRLF, "No communication with the PicoAmmeter Keithley 6485 1252668." & @CRLF & "Please check log for details" & @CRLF  )
	EndIf
	If $com_port_picoam_4005642 = -1 Then
		MsgBox($MB_ICONWARNING,"Device(s) not found"& @CRLF, "No communication with the PicoAmmeter Keithley 6485 4005642" & @CRLF & "Please check log for details" & @CRLF  )
	EndIf

	If $com_port_picoam_2614B = -1 Then
		MsgBox($MB_ICONWARNING,"Device(s) not found"& @CRLF, "No communication with the PicoAmmeter Keithley 2614B" & @CRLF & "Please check log for details" & @CRLF  )
	EndIf
	If $com_port_HV_1= -1 Then
		MsgBox($MB_ICONWARNING,"Device(s) not found"& @CRLF, "No communication with the HV_Left Module." & @CRLF & "Please check log for details" & @CRLF  )
	EndIf
	Return 0
EndFunc   ;==>port_identify

Func snd($msg)
	;Print("LOG (W): " & $msg & @CRLF)
	_CommSendString($msg & @LF, 1) ; wait for send
	Sleep(20)
EndFunc   ;==>snd


Func pico_read()
	$r = _CommGetLine(@LF, 10000, 8000) ; read max 10k chars, timeout 8s
	$e = @error
	;Print("LOG (R): >" & $r & "<" & @CRLF)
	;if ( $r = "") then
	;	Print("Error from CommGetLine: " & $e & @CRLF)
	;	 _CommGetLine(@LF, 10000, 100) ; read max 10k chars, timeout 100ms
	;	Print("LOG (R): retry >" & $r & "<" & @CRLF)
	;endif
	Return $r
EndFunc   ;==>pico_read



Func read_keithley_2614B($channel)

	$data = keithley_2614B_serial_reading($channel)
	$data = StringReplace($data, @LF, ",")

	Local $average=0
	Local $STD =0
	Local $dataArray = StringSplit($data, ",")
	Local $current_points = $dataArray[0]-1

	For $i = 1 To $current_points
		;; DEBUG STRINGS
		;;Print("Data["&$i&"] = " & $dataArray[$i]&@CRLF)

		$average += $dataArray[$i]
		$STD += $dataArray[$i]*$dataArray[$i]
	Next

	$average = -$average/$current_points
	$STD = Sqrt( $STD/$current_points - $average * $average )

	Local $HV1current = GetHVcurrent(1)
	$HV1current = Round($HV1current)
	Local $HV1Read = GetHVVoltage(1)
	$HV1Read = Number($HV1Read)
	$HV1Read = Abs($HV1Read)

	If $channel = "b"  Then
		Print(@CRLF&@CRLF&"Current Readout from Keithley 2614B-ChannelB. (ieta,iphi) = (6,1)")
	ElseIf $channel = "a" Then
		Print(@CRLF&@CRLF&"Current Readout from Keithley 2614B-ChannelA. (ieta,iphi) = (6,3)")
	EndIf
	Print(@CRLF & "=========" & @CRLF &"HV Param" & @CRLF&"Voltage = " & $HV1Read & "V     Current = " & $HV1current & "uA"   & @CRLF&"=========" & @CRLF)
	Print("Readout Current: Average = " & $average & @CRLF )
	Print("Readout Current: STD = " & $STD &@CRLF )
	Print("========================================"& @CRLF)

	Local $result[2] = [$average , $STD]
	Return $result

EndFunc ;;read_keithley_2614B


Func keithley_2614B_serial_reading($channel)  ;;channel can be either "a" or "b"
	Dim $returning_array[200]

	$port = $com_port_picoam_2614B
	$err_msg = "Setport failed"
	_CommSwitch($port)
	_CommSetport($port, $err_msg, 57600, 8, 0, 1, 0, 0, 0)


	snd("*RST") ; Return to RST defaults.
	Sleep(200)
	; get IDN
	snd("display.clear()")


	snd("smu"&$channel&".measure.autorangei = 1")
	sleep(200)
	snd("smu"&$channel&".measure.count = 200")
	sleep(200)
	snd("smu"&$channel&".nvbuffer1.clear()")
	sleep(200)
	snd("display.setcursor(1, 1)")
	sleep(200)
	snd("display.settext('$BPlease wait.$R$NMeasure operation in progress.')")
	sleep(200)
	snd("smu"&$channel&".measure.i(smu"&$channel&".nvbuffer1)")
	sleep(200)
	snd("display.clear()")
	sleep(200)
	$string =@HOUR &":"& @MIN&":"& @SEC&' Ch' &  $channel &'$NRedoutComplete'
	snd("display.settext('"&@HOUR&":"&@MIN&":"&@SEC&" Ch. "&StringUpper($channel)&"$NCurrent Readout Complete')")
	sleep(200)
	snd("printbuffer(1,smu"&$channel&".nvbuffer1.n,smu"&$channel&".nvbuffer1)")
	sleep(200)


	Local $data = pico_read()

	_Commcloseport(True)

	return $data
EndFunc ;;End-->keithley_2614B_serial_reading()



Func read_keithley_6485($port_to_read,$picoAmmRange)
	Local $HV1current = GetHVcurrent(1)
	$HV1current = Round($HV1current)
	Local $HV1Read = GetHVVoltage(1)
	$HV1Read = Number($HV1Read)
	$HV1Read = Abs($HV1Read)


	$setcurrent = $inputCurrent
	$xray_status = $inputXrayStatus ; Run Picoam data for QC5 measurement (default)


	$read_data = 0
	$current_points = 98
	$user_message= @CRLF &"========================================"& @CRLF
	Do
		Print($user_message)
		$d_array = Set_and_Read_keithley($current_points,$port_to_read,$picoAmmRange) ;; each current meas returns 3 float : current, timestamp and 0.0000. Therefore requesting $current_points returns an array of 3 x $current_points float
		$number_of_retrieved_data = UBound($d_array)/3
		Print("UBound($d_array)/3 = " & UBound($d_array)/3  & @CRLF)
		$user_message="Read out failed. "&@CRLF&"Reading AGAIN picoAmmeter data" &@CRLF
	Until $number_of_retrieved_data = $current_points
	Print("========================================"& @CRLF& @CRLF)
	; fill
	Dim $strom_array[$current_points]
	Dim $time_array[$current_points]
	Dim $status_array[$current_points]
	For $j = 0 To $current_points - 1 Step 1
		$strom_array[$j] = StringReplace(StringReplace($d_array[3 * $j], "A", ""), "+", "")
		$time_array[$j] = $d_array[3 * $j + 1]
		$status_array[$j] = $d_array[3 * $j + 2]
	Next

	Local $average = 0
	Local $STD = 0

	$file = FileOpen($filename, 2)
	For $j = 0 To $current_points - 1 Step 1
		;Print($time_array[$j] & "  " & $strom_array[$j] & @CRLF)
		FileWrite($file, $strom_array[$j] & @TAB & $time_array[$j] & @CRLF)

		$average += $strom_array[$j]
		$STD += $strom_array[$j]*$strom_array[$j]
	Next
	FileClose($file)

	$average = $average/$current_points
	$STD = Sqrt( $STD/$current_points - $average * $average )

	$HV1current = GetHVcurrent(1)
	$HV1current = Round($HV1current)
	$HV1Read = GetHVVoltage(1)
	$HV1Read = Number($HV1Read)
	$HV1Read = Abs($HV1Read)

	If $port_to_read = $com_port_picoam_4005642 Then
		Print(@CRLF&@CRLF&"Current Readout from Keithley 4005642. (ieta,iphi) = (2,2)")
	ElseIf $port_to_read = $com_port_picoam_1252668 Then
		Print(@CRLF&@CRLF&"Current Readout from Keithley 1252668. (ieta,iphi) = (6,2)")
	EndIf
	Print(@CRLF & "=========" & @CRLF &"HV Param" & @CRLF&"Voltage = " & $HV1Read & "V     Current = " & $HV1current & "uA"   & @CRLF&"=========" & @CRLF)
	Print("Readout Current: Average = " & $average & @CRLF )
	Print("Readout Current: STD = " & $STD &@CRLF )
	Print("========================================"& @CRLF)


	_Commcloseport(True)


	Local $result[2] = [$average , $STD]
	Return $result
EndFunc   ;==>read_keithley_6485

Func Set_and_Read_keithley($number_of_points,$port_to_read,$currentRange)  ;; read $number_of_points current points
	; Common ranges $range == HIGH "20e-6"
	; Common ranges $range == LOW "2e-9"
	if $currentRange = "HIGH" Then
		$range = "20e-6"
	ElseIf $currentRange = "LOW" Then
		$range = "2e-9"
	Else
		$range = "20e-6"
	EndIf
	Print("PicoAmm Range = " & $range & @CRLF)

	$port = $port_to_read
	$err_msg = "Setport failed"
	_CommSwitch($port)
	_CommSetport($port, $err_msg, 57600, 8, 0, 1, 0, 0, 0)

	snd("*RST") ; Return 6485 to RST defaults.
	Sleep(200)
	; get IDN
	snd("*IDN?")
	$idn = pico_read() ;;read the answer to the previous question
	Print("IDN: " & $idn)

	snd("TRIG:DEL 0") ; Set trigger delay to zero seconds
	snd("TRIG:COUNT " & $number_of_points) ; Set trigger count to $number_of_points
	snd("SENS:CURR:RANG:AUTO OFF") ; Turn auto-range off
	snd("SENS:CURR:RANG " & $range) ; Use custom range
	snd("NPLC 1") ; Set integration rate to 0.01. Power Lince Cycle (PLC)  --> Selects the integration time of the A/D converter, which is the period of time the input signal is measured. The integration time affects the amount of reading noise, as well as the ultimate reading rate of the instrument
	snd("SYST:ZCH OFF") ; Turn zero check off
	snd("SYST:AZER:STAT OFF") ; Turn auto-zero off
	snd("DISP:ENAB OFF") ; Turn display off
	snd("*CLS") ; Clear status model
	snd("TRAC:POIN " & $number_of_points) ; Set buffer size to $number_of_points
	snd("TRAC:CLE") ; Clear buffer
	snd("TRAC:FEED:CONT NEXT") ; Set storage control to start on next reading
	snd("STAT:MEAS:ENAB 512") ; Enable buffer full measurement event
	snd("*SRE 1") ; Enable SRQ on buffer full measurement event
	snd("*OPC?") ; operation complete query (synchronize completion of commands)

	$opc = pico_read()
	Print("Operation complete query = " & $opc )
	While $opc <> 1
		Sleep(200)
		Print("Operation complete query = " & $opc)
		snd("*OPC?")
		$opc = pico_read()
	WEnd


	snd("INIT") ; start taking and storing readings
	;wait for GPIB SRQ line to go true

;~ 	snd("*STB?") ;;Reads the status byte register.
;~ 	$srq = pico_read()
;~ 	While (StringStripWS($srq, 8) <> "65")  ;; 65 is the status we want. it means Measurement Done && Master Summary Status=1 (https://www.tek.com/document/application-note/how-program-instrument-assert-srq-gpib-bus)
;~ 		snd("*STB?")
;~ 		Sleep(500) ;
;~ 		$srq = pico_read()
;~ 	WEnd
;~ 	Print("STB = " & $srq)

	sleep(1000)

	snd("DISP:ENAB ON") ; turn display back on
	snd("TRAC:DATA?") ; Request data from buffer
	Sleep(100)

	;;read current point
	$data = ""
	$newbytes = "1"
	While $newbytes <> ""   ;; newbytes == "" means that all the data in the buffer have been read
		$newbytes = pico_read()
		If $newbytes = ""  Then ExitLoop
		$data = $data & $newbytes
	WEnd
	$d_array = StringSplit($data, ",", 2)
	_Commcloseport(True)
	Return $d_array

EndFunc ;=>Set_and_Read_keithley
